MPI локально может быть установлен для следующих ОС:

* Ubuntu: `sudo apt-get install openmpi-bin libopenmpi-dev`
* Mac OS: `brew install open-mpi`

Документация

* https://www.open-mpi.org/doc/current/ - мануал OpenMPI 4.0
* https://mpitutorial.com - хороший tutorial по MPI с примерами.
