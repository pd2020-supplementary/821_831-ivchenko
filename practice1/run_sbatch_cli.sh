#! /usr/bin/env bash

module add mpi/openmpi4-x86_64
#sbatch -n 8 --comment="Hello world on MPI" run_local.sh
sbatch -n 16 --ntasks-per-node=4 --comment="Hello world on MPI per four nodes" run_local.sh
#sbatch run_slurm.sh
