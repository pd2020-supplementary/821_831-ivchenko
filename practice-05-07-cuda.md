Ссылка на репозиторий П. Ахтямова: **[Link](https://github.com/akhtyamovpavel/ParallelComputationExamples/tree/master/CUDA)**.

| Семинар | Разобранные задачи | Доп. материалы|
|-----------------|-------------|---------------|
| 4 | 01-intro {01-04}, 02-device-specs-benchmarks {01-02}  |[Векторные операции CUDA](https://course.ece.cmu.edu/~ece740/f13/lib/exe/fetch.php?media=seth-740-fall13-module5.1-simd-vector-gpu.pdf)|
|5| 03-memory-model {01,02}, 03.5-matrix-multiplication-example ||
|6| 04-reduction, 06-cublas, 07-pycuda ||
